package F28DA_CW2;

import org.jgrapht.graph.DefaultWeightedEdge;

public class Flight extends DefaultWeightedEdge {

	private String departure;
	private String departureTime;
	private String flightNumber;
	private String destination;
	private String arrivalTime;
	private String price;

	//Creating a Flight class to store the information of the graph's edges
	public Flight(String departure, String departureTime, String flightNumber, String destination, String arrivalTime, String price) {

		this.departure = departure.toLowerCase();
		this.departureTime = departureTime;
		this.flightNumber = flightNumber.toUpperCase();
		this.destination = destination.toLowerCase();
		this.arrivalTime = arrivalTime;
		this.price = price;
	}
	
	public String getDeparture() {
		return departure;
	}
	
	public String getDepartureTime() {
		return departureTime;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public String getArrivalTime() {
		return arrivalTime;
	}
	
	public String getPrice() {
		return price;
	}
}
