package F28DA_CW2;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class SkyRoute implements IRoute {
	
	private SimpleDirectedWeightedGraph<String, Flight> flightGraph;
	private String from;
	private String to;
	
	public SkyRoute(SimpleDirectedWeightedGraph<String, Flight> flightGraph, String from, String to) {
		this.flightGraph = flightGraph;
		this.from = from;
		this.to= to;
	}

	@Override
	public List<String> getStops() {
		List<String> stops = DijkstraShortestPath.findPathBetween(flightGraph, from, to).getVertexList();
		return stops;
	}

	@Override
	public List<String> getFlights() {
		List<String> flights = DijkstraShortestPath.findPathBetween(flightGraph, from, to).getVertexList(); //Create a list of the airports in the path
		List<String> codes = new ArrayList<String>(); //Create new list to store the flight codes
		for(int i = 0; i < flights.size() - 1; i++) { //Loop through all the edges
			String start = flights.get(i); //Get first airport in loop
			String end = flights.get(i+1); //Get second airport in loop
			Flight path = flightGraph.getEdge(start, end); //Get the edge information from the Flight class
			codes.add(path.getFlightNumber()); //Add the flight number to the codes list
		}
		return codes;
	}

	@Override
	public int totalHop() {
		List<String> hop = DijkstraShortestPath.findPathBetween(flightGraph, from, to).getVertexList();
		return hop.size() - 1; //Number of airports stopped at -1 to account for the extra airport
	}

	@Override
	public int totalCost() {
		return (int) DijkstraShortestPath.findPathBetween(flightGraph, from, to).getWeight();
	}

	@Override
	public int airTime() {
		List<String> flights = DijkstraShortestPath.findPathBetween(flightGraph, from, to).getVertexList(); //Create a list of the airports in the path
		int airTime = 0; //Create airTime variable
		for(int i = 0; i < flights.size() - 1; i++) { //Loop through edges
			
			String start = flights.get(i); //Get first airport in loop
			String end = flights.get(i+1); //Get second airport in loop
			
			Flight path = flightGraph.getEdge(start, end); //Get edge from flight class using airports
			int startTime = Integer.parseInt(path.getDepartureTime()); //Get the departure time and make it an integer
			int endTime = Integer.parseInt(path.getArrivalTime()); //Get the arrival time and make it an integer
			
			//Separate the time into hours and minutes
			int startTimeHours = startTime / 100; //Remove minutes leaving hours
			int startTimeMinutes = startTime % 100; //Remove hours leaving minutes
			int endTimeHours = endTime / 100; //Remove minutes leaving hours
			int endTimeMinutes = endTime % 100; //Remove hours leaving minutes
			
			if(startTime > endTime) { //If the flight goes into the next day
				int sumHours = (24 - startTimeHours + endTimeHours) * 60; //24 hours take off the departure time and add the arrival time to get time between start and finish
				int sumMinutes = (endTimeMinutes - startTimeMinutes);
				airTime += sumHours; //Add to variable
				airTime += sumMinutes; //Add to variable
			} else {
				int sumHours = (endTimeHours - startTimeHours) * 60;
				int sumMinutes = endTimeMinutes - startTimeMinutes; //Arrival time take off departure time
				airTime += sumHours; //Add to variable
				airTime += sumMinutes; //Add to variable
			}
		}
		return airTime;
	}

	@Override
	public int connectingTime() {
		List<String> flights = DijkstraShortestPath.findPathBetween(flightGraph, from, to).getVertexList(); //Create a list of the airports in the path
		int connectionTime = 0; //Create airTime variable
		
		for(int i = 0; i < flights.size() - 2; i++) { //Loop through edges
			String start = flights.get(i); //Get first airport in loop
			String middle = flights.get(i + 1); //Get second airport in loop
			String end = flights.get(i + 2); //Get thirs airport in loop
			
			Flight leg1 = flightGraph.getEdge(start, middle); //Get edge of first flight from flight class using airports
			Flight leg2 = flightGraph.getEdge(middle, end); //Get edge of second flight from flight class using airports
			int startTime = Integer.parseInt(leg1.getArrivalTime()); //Get the arrival time of first flight and make it an integer
			int endTime = Integer.parseInt(leg2.getDepartureTime()); //Get the departure time of second flight and make it an integer
			
			//Separate the time into hours and minutes
			int startTimeHours = startTime / 100; //Remove minutes leaving hours
			int startTimeMinutes = startTime % 100; //Remove hours leaving minutes
			int endTimeHours = endTime / 100; //Remove minutes leaving hours
			int endTimeMinutes = endTime % 100; //Remove hours leaving minutes
			
			if(startTime > endTime) { //If the flight goes into the next day
				int sumHours = (24 - startTimeHours + endTimeHours) * 60;
				int sumMinutes = (endTimeMinutes - startTimeMinutes);
				connectionTime += sumHours; //Add to variable
				connectionTime += sumMinutes; //Add to variable
			} else {
				int sumHours = (endTimeHours - startTimeHours) * 60;
				int sumMinutes = endTimeMinutes - startTimeMinutes; //Arrival time take off departure time
				connectionTime += sumHours; //Add to variable
				connectionTime += sumMinutes; //Add to variable
			}
		}
		return connectionTime;
	}

	@Override
	public int totalTime() {
		return airTime() + connectingTime();
	}

}
