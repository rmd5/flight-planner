package F28DA_CW2;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class SkyRoutes implements IRoutes {

	// TO IMPLEMENT
	
	private static SimpleDirectedWeightedGraph<String, Flight> flightGraph = new SimpleDirectedWeightedGraph<>(Flight.class);

	public static void partA() {
		
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> flightGraph = new SimpleDirectedWeightedGraph<>(DefaultWeightedEdge.class);
		
		String v1 = "Edinburgh";
		String v2 = "Heathrow";
		String v3 = "Dubai";
		String v4 = "Sydney";
		String v5 = "Kuala Lumpur";
		
		//Add the airports as vertices
		flightGraph.addVertex(v1);
		flightGraph.addVertex(v2);
		flightGraph.addVertex(v3);
		flightGraph.addVertex(v4);
		flightGraph.addVertex(v5);
		
		DefaultWeightedEdge edge = new DefaultWeightedEdge();
		
		//Route from Edinburgh to Heathrow costing \u00A380
		edge = flightGraph.addEdge(v1, v2);
		flightGraph.setEdgeWeight(edge, 80);
		edge = flightGraph.addEdge(v2, v1);
		flightGraph.setEdgeWeight(edge, 80);
		
		//Route from Heathrow to Dubai costing \u00A3130
		edge = flightGraph.addEdge(v2, v3);
		flightGraph.setEdgeWeight(edge, 130);
		edge = flightGraph.addEdge(v3, v2);
		flightGraph.setEdgeWeight(edge, 130);
		
		//Route from Heathrow to Sydney costing \u00A3570
		edge = flightGraph.addEdge(v2, v4);
		flightGraph.setEdgeWeight(edge, 570);
		edge = flightGraph.addEdge(v4, v2);
		flightGraph.setEdgeWeight(edge, 570);
		
		//Route from Dubai to Kuala Lumpur costing \u00A3170
		edge = flightGraph.addEdge(v3, v5);
		flightGraph.setEdgeWeight(edge, 170);
		edge = flightGraph.addEdge(v5, v3);
		flightGraph.setEdgeWeight(edge, 170);
		
		//Route from Dubai to Edinburgh costing \u00A3190
		edge = flightGraph.addEdge(v3, v1);
		flightGraph.setEdgeWeight(edge, 190);
		edge = flightGraph.addEdge(v1, v3);
		flightGraph.setEdgeWeight(edge, 190);
		
		//Route from Kuala Lumpur to Sydney costing \u00A3150
		edge = flightGraph.addEdge(v5, v4);
		flightGraph.setEdgeWeight(edge, 150);
		edge = flightGraph.addEdge(v4, v5);
		flightGraph.setEdgeWeight(edge, 150);
		
		Scanner scanner = new Scanner(System.in);
		
		//Print out the codes of all the airports on offer
		System.out.println("We provide flights between the following cities:");
		System.out.println(flightGraph.vertexSet().toString());
		
		//Scan in the starting and ending vertex
		System.out.println("Please enter your starting destination:");
		String startVertex = scanner.nextLine();
		System.out.println("Please enter your destination of travel:");
		String endVertex = scanner.nextLine();
		scanner.close();
		
		System.out.println("The cheapest route between " + startVertex + " and " + endVertex + " is:");
		
		//Find the shortest path between two vertices and return the path and the cost
		String path = null;
		double pathCost = 0;
		if (DijkstraShortestPath.findPathBetween(flightGraph, startVertex, endVertex) != null) {
			
			//Sets the path to be equal to the String of the cheapest path
			path = DijkstraShortestPath.findPathBetween(flightGraph, startVertex, endVertex).toString();
			
			//Sets the pathCost to be equal to the combined weight of the edges
			pathCost = DijkstraShortestPath.findPathBetween(flightGraph, startVertex, endVertex).getWeight();
			
		} else {
			System.out.println("No Path Found");
			System.exit(0);
		}
		
		//Print the path and the cost
		System.out.println(path);
		System.out.println("The ticket cost is:\n\u00A3" + pathCost);
	}

	public static void partB() throws FileNotFoundException, SkyRoutesException {
		
		//This is the code we were given
		IRoutes fi = new SkyRoutes();
		FlightsReader fr;
		try {
			
			fr = new FlightsReader(FlightsReader.MOREAIRLINECODES);
			fi.populate(fr.getAirlines(), fr.getAirports(), fr.getFlights());
			
			Scanner scanner = new Scanner(System.in);
			
			//Scan in the starting and ending vertex
			System.out.println("We provide flights between the following airports:");
			System.out.println(flightGraph.vertexSet().toString());
			System.out.println("Please enter your starting destination:");
			String startVertex = scanner.nextLine();
			
			if(flightGraph.containsVertex(startVertex)){
				
				System.out.println("Please enter your destination of travel:");
				String endVertex = scanner.nextLine();
				
				if(flightGraph.containsVertex(endVertex)){
					
					System.out.println("The cheapest path we have found for you is:");
					System.out.format("%-5s %-7s %-5s %-8s %-8s %-6s %-7s", "Leg", "Leave", "At", "On", "Arrive", "At", "Price"); //Format the header
					System.out.println();
					IRoute i = fi.leastCost(startVertex, endVertex); //Call the leastCost method to print out the information of the cheapest path
					
				} else {
					
					System.out.println("This airport does not exist! Try Again");
					System.out.println();
					partB(); //Repeat partB if the airport input is not found in the graph
					
				}
			} else {
				
				System.out.println("This airport does not exist! Try Again");
				System.out.println();
				partB();
				
			}
			
			scanner.close();
			
		} catch (FileNotFoundException | SkyRoutesException e){
			e.printStackTrace();
		}
	}

	public static void partC() {
		// TO IMPLEMENT
	}

	public static void main(String[] args) throws FileNotFoundException, SkyRoutesException {
		//partA();
		partB();
		//partC();
	}

	@Override
	public boolean populate(HashSet<String[]> airlines, HashSet<String[]> airports, HashSet<String[]> flights) {
		
		//Create iterators to filter through the two HashSets
//		Iterator<String[]> airportIterator = airports.iterator();
//		Iterator<String[]> flightsIterator = flights.iterator();
		
		//Iterate through the airports HashSet taking the first element of each array and adding it to the graph as a vertex
//		while(airportIterator.hasNext()){
			for(String[] x : airports) { //Retrieve elements from the String arrays within the airports HashSet
				flightGraph.addVertex(x[0]); //0 is the location of the airport code
			}
//			airportIterator.next();
//		}
		
		//Iterate through the flights HashSet creating a new Flight object
		//add the information as an edge and set the edge weight to represent cost
//		while(flightsIterator.hasNext()){
			for(String[] y : flights) { //Retrieve elements from the String arrays within the flights HashSet
				
				//1: Departure airport 2: Departure time 0: Flight code 3: Arrival airport 4: Arrival time 5: Price
				Flight flight = new Flight(y[1],y[2],y[0],y[3],y[4],y[5]);
				flightGraph.addEdge(y[1], y[3], flight); //storing the Flight info in the edge
				flightGraph.setEdgeWeight(flight, Integer.parseInt(y[5])); //Setting the price for the flight
			}
//			flightsIterator.next();
//		}
		
		//All the loops have run through so return true
		//This method works
		//System.out.print(flightGraph); //Prints out all the airport codes and all the connections between them
		return true;
	}

	@Override
	public IRoute leastCost(String from, String to) throws SkyRoutesException {
		IRoute route = new SkyRoute(flightGraph, from, to);
		List<String> list = route.getStops(); //Creates a list of the stops on the journey
		String poundSign = "\u00A3"; //To enter into table
		
		if(list.isEmpty()) {
			System.out.println("Unfortunately we don't offer flights between these two airports! Try again");
			return null;
		} else {
			for(int i = 0; i < list.size() - 1; i++) { //Loop through the size of the list
				
				String startVertex = list.get(i); //Get first value in the list in the loop
				String endVertex = list.get(i+1); //Get second value in the list in the loop
				Flight path = flightGraph.getEdge(startVertex, endVertex); //Finds the edge between the two airports
				
				//Formats and prints all the information of the flight
				System.out.format("%-5s %-7s %-5s %-8s %-8s %-6s %-1s %-7s",
						String.valueOf(i+1), 
						String.valueOf(path.getDeparture().toUpperCase()), 
						String.valueOf(path.getDepartureTime()), 
						String.valueOf(path.getFlightNumber()), 
						String.valueOf(path.getDestination().toUpperCase()), 
						String.valueOf(path.getArrivalTime()),
						String.valueOf(poundSign),
						String.valueOf(path.getPrice()));
				System.out.println(); //Drops a line before printing out the next leg
			}
			
			System.out.println("For a total price of: \u00A3" + route.totalCost()); //Print out the total cost of the journey
			System.out.println("Total amount of air time: " + minutesToHours(route.airTime())); //Print out the total air time of the journey
			System.out.println("Total amount of connection time: " + minutesToHours(route.connectingTime())); //Print out the total connection time of the journey
			System.out.println("Total journey time: " + minutesToHours(route.totalTime())); //Print out the total time of the journey
			return route;
		}
	}
	
	public String minutesToHours(int minutes) {
		return String.format("%02d days %02d hours %02d minutes", (minutes / 60) / 24, (minutes / 60) % 24, minutes % 60);
	}
	
	@Override
	public IRoute leastHop(String from, String to) throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IRoute leastCost(String from, String to, List<String> excluding) throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IRoute leastHop(String from, String to, List<String> excluding) throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String leastCostMeetUp(String at1, String at2) throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String leastHopMeetUp(String at1, String at2) throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String leastTimeMeetUp(String at1, String at2, String startTime) throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IRoute> allRoutesCost(String from, String to, List<String> excluding, int maxCost)
			throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IRoute> allRoutesHop(String from, String to, List<String> excluding, int maxHop)
			throws SkyRoutesException {
		// TODO Auto-generated method stub
		return null;
	}
}